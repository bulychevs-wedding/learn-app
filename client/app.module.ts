import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { provideAuth, JwtHelper } from 'angular2-jwt';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { NgSemanticModule } from 'ng-semantic';

import { AppComponent }  from './app.component';
import { SharedComponentsModule }  from './components/shared/shared.module';
import { routing } from './routes';
import { CoursesModule } from './modules/courses/courses.module';
import { HomeModule } from './modules/home/home.module';
import { SharedModule } from './modules/shared/shared.module';
import {PupilModule} from "./modules/pupils/pupil.module";


@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        NgSemanticModule,
        CoursesModule,
        PupilModule,
        HomeModule,
        routing,
        SharedComponentsModule,
        SharedModule.forRoot(),

    ],
    providers: [
        provideAuth({
            globalHeaders: [{'Content-type': 'application/json'}],
            newJwtError: true,
            noTokenScheme: true
        }),
        JwtHelper
    ],
    declarations: [ AppComponent ],
    bootstrap:    [ AppComponent ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ]
})
export class AppModule {}
