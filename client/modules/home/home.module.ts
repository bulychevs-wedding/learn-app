import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {HttpModule} from "@angular/http";
import {NgSemanticModule} from "ng-semantic";
import {CommonModule} from "@angular/common";
import { FormsModule } from "@angular/forms";

import {HomeComponent} from "./home.component";
import {SharedComponentsModule} from '../../components/shared/shared.module';
import {SharedModule} from "../shared/shared.module";

@NgModule({
	imports: [
		SharedComponentsModule,
		CommonModule,
		HttpModule,
		SharedModule.forRoot(),
		NgSemanticModule,
		FormsModule
	],
	declarations: [
		HomeComponent
	],
	schemas: [
		CUSTOM_ELEMENTS_SCHEMA
	]
})
export class HomeModule {
}