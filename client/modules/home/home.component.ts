import {Component} from "@angular/core";
import {ApiService} from "../../service/api.service";
import {DomSanitizer} from "@angular/platform-browser";
import  "js-video-url-parser";

@Component({
	selector: "home",
	templateUrl: `client/modules/home/home.component.html`,
	styles: [`.home { background-image: url('bg.jpg')}`]
})
export class HomeComponent {
	error: string;
	response: {};
	model = {videoType:''};
	constructor(private apiService: ApiService, private sanitizer: DomSanitizer) {
	}

	protected() {
		this.apiService
			.get("/api")
			.subscribe(
				(data) => {
					this.response = data;
				},
				(error: Error) => {
					this.error = error.message;
					setTimeout(() => this.error = null, 4000)
				});
	}

	//
}
