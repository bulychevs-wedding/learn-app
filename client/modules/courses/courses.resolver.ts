import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { ApiService } from "../../service/api.service";

@Injectable()
export class CoursesResolver implements Resolve<any> {
	constructor(
		private apiService: ApiService
	) {}

	resolve(route: ActivatedRouteSnapshot): Observable<any> {
		console.log('resolver');
		return this.apiService.get('/api/courses');
	}
}