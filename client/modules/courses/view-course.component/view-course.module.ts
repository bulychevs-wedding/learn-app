import { NgModule } from '@angular/core';
import { NgSemanticModule } from "ng-semantic";
import { FormsModule } from "@angular/forms";
import {CommonModule} from "@angular/common";

import { ViewCourseComponent } from "./view-course.component";
import {SharedComponentsModule} from '../../../components/shared/shared.module';
import LessonsModule from "../../lessons/lessons.module";
import {RouterModule} from "@angular/router";


@NgModule({
    imports: [
        LessonsModule,
        SharedComponentsModule,
        NgSemanticModule,
        FormsModule,
        CommonModule,
        RouterModule
    ],
    declarations: [ ViewCourseComponent]
})
export default class ViewCoursesModule { }