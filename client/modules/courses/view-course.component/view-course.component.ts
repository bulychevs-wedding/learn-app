import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute, Params} from '@angular/router';
import {ApiService} from "../../../service/api.service";
import {DomSanitizer} from "@angular/platform-browser";
import 'rxjs/add/operator/switchMap';
import {Validators, FormControl} from "@angular/forms";
import {UserService} from "../../../service/user.service";

@Component({
	selector: "view-course",
	templateUrl: `client/modules/courses/view-course.component/view-course.component.html`
})
export class ViewCourseComponent implements OnInit{
	ngOnInit(): void {
		this.apiService.get('/api/courses/' + this.route.snapshot.params.courseId)
			.subscribe(course => this.course = course);

		if (this.isTeacher()) {
			this.apiService
				.get(`/api/pupils/my`)
				.subscribe(pupils => {
					this.pupils = pupils;
				}, error => console.error(error));
		} else {
			this.apiService.get(`api/courses/info/?userId=${this.userService.userInfo.userId}&courseId=${this.route.snapshot.params.courseId}`)
				.subscribe(course => {
					this.currentCourseIsFinished = course && course.isFinished || false;
				});
		}

	}
	pupils = [];
	selectControl: FormControl = new FormControl('', Validators.required);
	course;
	selectedPupils = [];
	currentCourseIsFinished = false;

	onSelectPupil(data: Array<string>) {
		this.selectedPupils = data;
	}

	suggestPupil() {
		this.apiService
			.put('api/pupils/suggest', {
				courseId: this.route.snapshot.params.courseId,
				pupilIds: this.selectedPupils
			})
			.subscribe(data => console.log(data));
	}
	isTeacher() {
		return this.userService.isTeacher();
	}

	isInCreate(): boolean{
		return this.route.snapshot.fragment && this.route.snapshot.fragment.includes('create');
	}
	constructor(
		private apiService: ApiService,
		private route: ActivatedRoute,
	  private userService: UserService) {
	}
}
