import {Component, OnInit} from "@angular/core";
import {ApiService} from "../../service/api.service";
import {Router, ActivatedRoute} from "@angular/router";
import {UserService} from "../../service/user.service";

@Component({
	selector: "courses",
	templateUrl: `client/modules/courses/courses.component.html`
})
export class CoursesComponent implements OnInit {
	courses = [];
	currentCourses = [];
	finishedCourses = [];

	ngOnInit(): void {
		console.log('onInit');
		// this.courses = this.route.snapshot.data.courses;
		this.route.data.map(d => {
			console.info(d);
			return d.courses;
		})
			.subscribe(courses => this.courses = courses, error => console.error(error));
		if (!this.isTeacher()) {
			this.apiService.get(`api/courses/info/?userId=${this.userService.userInfo.userId}`)
				.subscribe(pupil => {
					this.currentCourses = pupil.currentCourses;
					this.finishedCourses = pupil.recentCourses;
				});
		}
	}

	viewCourse(course) {
		this.router.navigate([`courses/${course._id}/lessons`]);
	}

	removeCourse(course) {
		this.apiService
			.delete('/api/courses/' + course._id)
			.subscribe(
				(courses) => this.courses = courses,
				error => console.error(error)
			);
	}

	addNew() {
		this.router.navigate(['./create'], {relativeTo: this.route});
	}

	isCurrentCourse(course) {
		return this.currentCourses.some(current => current.courseData === course._id);
	}

	isFinishedCourse(course) {
		return this.finishedCourses.some(finished => finished._id === course._id);

	}

	isAuthor(course): boolean {
		return this.userService.isAuthor(course.author);
	}

	isTeacher() {
		return this.userService.isTeacher();
	}

	constructor(private apiService: ApiService,
	            private router: Router,
	            private route: ActivatedRoute,
	            private userService: UserService) {
	}
}
