import {Component, EventEmitter, Output} from "@angular/core";
import {ApiService} from "../../../service/api.service";
import  "js-video-url-parser";
import {Router} from "@angular/router";

@Component({
	selector: "creat-course",
	templateUrl: `client/modules/courses/create-course.component/create-course.component.html`
})
export class CreateCourseComponent {
	error: string;
	targetUrl: string;
	model = {videoType: ''};
	@Output() onCreated = new EventEmitter();

	constructor(private apiService: ApiService, private router: Router) {
	}

	saveCourse(model) {

		if (model.name) {
			let savingData = {
				name: model.name,
				description: model.description,
			};
			this.apiService.post('/api/courses', savingData)
				.subscribe(
					() => this.router.navigate(['/courses']),
					error => console.error(error)
				);
		}
	}

}
