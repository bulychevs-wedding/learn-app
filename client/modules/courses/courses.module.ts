import { NgModule } from '@angular/core';
import { NgSemanticModule } from "ng-semantic";

import { CoursesComponent } from "./courses.component";
import { CreateCoursesModule } from "./create-course.component/create-course.module";
import { CoursesResolver } from "./courses.resolver";
import {CommonModule} from "@angular/common";
import ViewCoursesModule from "./view-course.component/view-course.module";


@NgModule({
    imports: [
        CreateCoursesModule,
        ViewCoursesModule,
        NgSemanticModule,
        CommonModule,
    ],
    providers: [CoursesResolver],
    declarations: [ CoursesComponent ]
})
export class CoursesModule { }