import { NgModule } from '@angular/core';
import { NgSemanticModule } from "ng-semantic";

import { LessonsComponent } from "./lessons.component";
import { CreateLessonsModule } from "./create-lesson.component/create-lesson.module";
import {CommonModule} from "@angular/common";
import {ViewLessonsModule} from "./view-lesson.component/view-lesson.module";


@NgModule({
    imports: [
        CreateLessonsModule,
        ViewLessonsModule,
        NgSemanticModule,
        CommonModule
    ],
    declarations: [ LessonsComponent ]
})
export default class LessonsModule { }