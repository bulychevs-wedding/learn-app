import { NgModule } from '@angular/core';
import { HttpModule } from "@angular/http";
import { NgSemanticModule } from "ng-semantic";
import { FormsModule } from "@angular/forms";
import {CommonModule} from "@angular/common";

import { ViewLessonComponent } from "./view-lesson.component";
import {SharedComponentsModule} from '../../../components/shared/shared.module';


@NgModule({
    imports: [
        SharedComponentsModule,
        HttpModule,
        NgSemanticModule,
        FormsModule,
        CommonModule,
    ],
    declarations: [ ViewLessonComponent]
})
export class ViewLessonsModule { }