import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute, Params} from '@angular/router';
import {ApiService} from "../../../service/api.service";
import {DomSanitizer} from "@angular/platform-browser";
import  "js-video-url-parser";
import 'rxjs/add/operator/switchMap';
import {UserService} from "../../../service/user.service";

@Component({
	selector: "view-lesson",
	templateUrl: `client/modules/lessons/view-lesson.component/view-lesson.component.html`
})
export class ViewLessonComponent implements OnInit {
	ngOnInit(): void {
		this.route.params
		// (+) converts string 'id' to a number
			.switchMap((params: Params) => this.apiService.get('/api/lessons/' + params['lessonId']))
			.subscribe(lesson => {
				this.lesson = lesson;
				if (lesson.videoUrl) {
					this.lesson.videoUrl = <string>this.sanitizer.bypassSecurityTrustResourceUrl(lesson.videoUrl);
				}

				lesson.videoId = lesson.videoId ? `/files/${lesson.videoId}` : undefined;
			});
	}

	isTeacher(): boolean{
		return this.userService.isTeacher();
	}

	markAsLearned() {
		this.apiService
			.put(`api/lessons/${this.route.snapshot.params.lessonId}/learned`, {
				courseId: this.route.snapshot.parent.parent.params.courseId
			})
			.subscribe(course => console.log(course));
	}

	error: string;
	lesson;
	model = {videoType: ''};

	constructor(private apiService: ApiService,
	            private sanitizer: DomSanitizer,
							private userService: UserService,
	            private route: ActivatedRoute) {
	}
}
