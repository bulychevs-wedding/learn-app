import {Component, OnInit,} from "@angular/core";
import {ApiService} from "../../service/api.service";
import {Router, ActivatedRoute} from "@angular/router";
import {UserService} from "../../service/user.service";

@Component({
	selector: "lessons",
	templateUrl: `client/modules/lessons/lessons.component.html`
})
export class LessonsComponent implements OnInit {
	lessons;
	course = {};
	currentCourse = {
		finishedLessons: []
	};

	ngOnInit(): void {
		this.apiService
			.get(`/api/courses/${this.route.snapshot.parent.parent.params.courseId}/lessons`)
			.subscribe(course => {
				this.course = course;
				this.lessons = course.lessons;
			}, error => console.error(error));

		if (!this.isTeacher()) {
			this.apiService.get(`api/courses/info/?userId=${this.userService.userInfo.userId}&courseId=${this.route.snapshot.parent.parent.params.courseId}`)
				.subscribe(course => {
					this.currentCourse.finishedLessons = course && course.finishedLessons || [];
				});
		}
	}

	viewLesson(lesson) {
		this.router.navigate(['./' + lesson._id], {relativeTo: this.route});
	}

	addNew() {
		this.router.navigate(['./create'], {relativeTo: this.route});
	}

	removeLesson(lesson) {
		this.apiService.delete('/api/lessons/' + lesson._id, {
			courseId: this.route.snapshot.params.courseId
		})
			.subscribe(
				lessons => this.lessons = lessons,
				error => console.error(error)
			);
	}

	isAuthor(): boolean {
		return this.userService.isAuthor(this.course.author);
	}

	isTeacher() {
		return this.userService.isTeacher();
	}

	isLessonFinished(lesson): boolean {
		return this.currentCourse.finishedLessons.some(finishedLesson => lesson._id === finishedLesson);
	}

	constructor(private apiService: ApiService,
	            private router: Router,
	            private route: ActivatedRoute,
	            private userService: UserService) {
	}
}
