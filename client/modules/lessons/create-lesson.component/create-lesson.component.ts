import {Component} from "@angular/core";
import {ApiService} from "../../../service/api.service";
import {DomSanitizer} from "@angular/platform-browser";
import  "js-video-url-parser";
import {Router, ActivatedRoute, Params} from "@angular/router";

@Component({
	selector: "creat-lesson",
	templateUrl: `client/modules/lessons/create-lesson.component/create-lesson.component.html`
})
export class CreateLessonComponent {
	error: string;
	response: {};
	srcFileUrl: string;
	fileId: string;
	targetUrl: string;
	model = {videoType: ''};

	constructor(private apiService: ApiService, private sanitizer: DomSanitizer, private router: Router,
	            private route: ActivatedRoute) {
	}

	protected() {
		this.apiService
			.get("/api")
			.subscribe(
				(data) => {
					this.response = data;
				},
				(error: Error) => {
					this.error = error.message;
					setTimeout(() => this.error = null, 4000)
				});
	}

	parseVideoUrl(currentUrl: string) {
		// let id = currentUrl.split('youtu.be/')[1];
		try {
			var videoInfo = urlParser.parse(currentUrl);
			var newUrl = urlParser.create({
				videoInfo,
				format: 'embed'
			});
		} catch (err) {
			this.targetUrl = undefined;
		}
		this.targetUrl = <string>this.sanitizer.bypassSecurityTrustResourceUrl(`https:${newUrl}`);
	}

	handleFileSuccess(file) {
		console.log(file);
		this.fileId = file.savedObject._id;
		this.srcFileUrl = `/files/${file.savedObject._id}`;
	}

	handleFileFailure() {

	}

	saveLesson(model) {

		if (model.videoType && model.name) {
			let data = {
				name: model.name,
				description: model.description,
			};

			switch (model.videoType) {
				case 'mp4':
					data.videoId = this.fileId;
					break;
				case 'youtube':
					data.videoUrl = this.targetUrl.changingThisBreaksApplicationSecurity;
					break;
			}
			data.courseId = this.route.snapshot.parent.parent.params.courseId;
			return this.apiService.post('/api/lessons', data)
				.subscribe(data => {
					console.log('saved lesson: ', data);
					this.router.navigate(['../'], {relativeTo: this.route}), error => console.error(error)
				});
		}
	}

}
