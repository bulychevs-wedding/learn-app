import { NgModule } from '@angular/core';
import { HttpModule } from "@angular/http";
import { NgSemanticModule } from "ng-semantic";
import { FormsModule } from "@angular/forms";
import {CommonModule} from "@angular/common";

import { CreateLessonComponent } from "./create-lesson.component";
import {SharedComponentsModule} from '../../../components/shared/shared.module';


@NgModule({
    imports: [
        SharedComponentsModule,
        HttpModule,
        NgSemanticModule,
        FormsModule,
        CommonModule
    ],
    declarations: [ CreateLessonComponent]
})
export class CreateLessonsModule { }