import {Component, OnInit} from "@angular/core";
import {ApiService} from "../../../service/api.service";
import {Observable} from "rxjs/Observable";

@Component({
	selector: "home",
	templateUrl: `client/modules/pupils/add-pupil/add-pupil.component.html`
})
export class AddPupilComponent implements OnInit{
	constructor(private apiService: ApiService) {
	}

	pupils;

	ngOnInit(): void {
		this.getFreePupils();
	}

	getFreePupils() {
		this.apiService
			.get(`/api/pupils/my`)
			.switchMap(this.filterPupils())
			.subscribe(pupils => {
				this.pupils = pupils;
			}, error => console.error(error));
	}

	private filterPupils() {
		return myPupils => this.apiService
			.get('/api/pupils')
			.map(pupils => {
				if (!myPupils.length) {
					return pupils;
				}
				return pupils.filter(pupil => !myPupils
					.some(myPupil => {
						return myPupil._id == pupil._id
					}))
			});
	}


	addPupil(pupil) {
		this.apiService
			.put(`/api/pupils/mentor`, {pupilId: pupil._id})
			.switchMap(this.filterPupils())
			.subscribe(pupils => {
				this.pupils = pupils;
			}, error => console.error(error));
	}

}
