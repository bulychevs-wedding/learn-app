import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {NgSemanticModule} from "ng-semantic";
import {CommonModule} from "@angular/common";
import { FormsModule } from "@angular/forms";

import {PupilComponent} from "./pupil.component";
import {AddPupilComponent} from "./add-pupil/add-pupil.component";
import {ViewPupilComponent} from "./view-pupil/view-pupil.component";

@NgModule({
	imports: [
		CommonModule,
		NgSemanticModule,
		FormsModule
	],
	declarations: [
		PupilComponent,
		AddPupilComponent,
		ViewPupilComponent
	],
	schemas: [
		CUSTOM_ELEMENTS_SCHEMA
	]
})
export class PupilModule {
}