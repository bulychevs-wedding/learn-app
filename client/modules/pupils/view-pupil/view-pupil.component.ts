import {Component, OnInit} from "@angular/core";
import {ApiService} from "../../../service/api.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
	selector: "home",
	templateUrl: `client/modules/pupils/view-pupil/view-pupil.component.html`
})
export class ViewPupilComponent implements OnInit{
	constructor(private apiService: ApiService,
	            private route: ActivatedRoute,
	            private router: Router) {
	}

	pupil = {
		auth: {},
		currentCourses: [],
		recentCourses: []
	};

	ngOnInit(): void {
		this.apiService.get(`api/pupils/${this.route.snapshot.params.pupilId}`)
			.subscribe(pupil => this.pupil = pupil);
	}

	viewCourse(course) {
		this.router.navigate([`/courses/${course._id}/lessons`]);
	}

}
