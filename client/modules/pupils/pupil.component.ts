import {Component, OnInit} from "@angular/core";
import {ApiService} from "../../service/api.service";
import {Router, ActivatedRoute} from "@angular/router";

@Component({
	selector: 'home',
	templateUrl: 'client/modules/pupils/pupil.component.html'
})
export class PupilComponent implements OnInit{
	constructor(private apiService: ApiService,
	            private router: Router,
							private route: ActivatedRoute) {
	}

	pupils;
	ngOnInit(): void {
		this.apiService
			.get(`/api/pupils/my`)
			.subscribe(pupils => {
				this.pupils = pupils;
			}, error => console.error(error));
	}

	viewPupil(pupil) {
		this.router.navigate([`/pupils/${pupil._id}`]);
	}
	addPupil() {
		this.router.navigate(['./add-pupil'],{relativeTo: this.route});
	}
}
