import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from "./modules/home/home.component";
import {CoursesComponent} from "./modules/courses/courses.component";
import {CoursesResolver} from "./modules/courses/courses.resolver";
import {CreateCourseComponent} from "./modules/courses/create-course.component/create-course.component";
import {ViewCourseComponent} from "./modules/courses/view-course.component/view-course.component";
import {LessonsComponent} from "./modules/lessons/lessons.component";
import {CreateLessonComponent} from "./modules/lessons/create-lesson.component/create-lesson.component";
import {ViewLessonComponent} from "./modules/lessons/view-lesson.component/view-lesson.component";
import {PupilComponent} from "./modules/pupils/pupil.component";
import {AddPupilComponent} from "./modules/pupils/add-pupil/add-pupil.component";
import {ViewPupilComponent} from "./modules/pupils/view-pupil/view-pupil.component";

export const routes: Routes = [
	{path: '', redirectTo: 'home', pathMatch: 'full'},
	{path: 'home', component: HomeComponent},
	{
		path: 'courses',
		// pathMatch: "full",
		// component: CoursesComponent,
		children: [
			{
				path: 'create',
				component: CreateCourseComponent
			},
			{
				path: ':courseId',
				component: ViewCourseComponent,
				children: [
					{
						path: 'lessons',
						children: [
							{
								path: 'create',
								component: CreateLessonComponent
							},
							{
								path: ':lessonId',
								component: ViewLessonComponent
							},
							{
								path: '',
								// pathMatch: 'full',
								// loadChildren: 'client/src/modules/lessons/lessons.module',
								component: LessonsComponent
							}
						]
					}
				]
				// loadChildren: 'client/src/modules/courses/view-course.component/view-course.module'
			},
			{
				path: '',
				component: CoursesComponent,
				resolve: {
					courses: CoursesResolver
				}
			}
		]
	},
	{
		path: 'pupils',
		children: [
			{
				path: '',
				component: PupilComponent
			},
			{
				path: 'add-pupil',
				component: AddPupilComponent
			},
			{
				path: ':pupilId',
				component: ViewPupilComponent
			}
		]
	}
];

export const routing = RouterModule.forRoot(routes, {useHash: true});