import {Component, OnInit, Input, Output, EventEmitter, ElementRef} from '@angular/core';
import {Http} from '@angular/http';
import Dropzone  from 'dropzone';

@Component({
	selector: 'dropzone',
	template: ``
})
/*Dropzone for file uploads*/
export class DropzoneComponent implements OnInit {

	private _dropzone: Dropzone;

	@Input()
	element: string = ".dropzoneArea";

	@Input()
	url: string = "files/upload";

	@Input()
	uploadMultiple: boolean = false;

	@Input()
	maxFileSize: number = 2048;

	@Input()
	clickable: string = ".dropzone";

	@Input()
	autoProcessQueue: boolean = true;

	@Input()
	addRemoveLinks: boolean = true;

	@Input()
	createImageThumbnails: boolean = true;

	// @Input()
	// previewTemplate: string = "<div style='display:none'></div>";

	@Input()
	acceptedFiles: string = "*";

	@Output()
	sending: EventEmitter<boolean>;

	@Output()
	uploadprogress: EventEmitter<number>;

	@Output()
	success: EventEmitter<any>;

	@Output()
	error: EventEmitter<any>;

	constructor(private _eleRef: ElementRef, private _http: Http) {

		this.sending = new EventEmitter<boolean>();
		this.uploadprogress = new EventEmitter<number>();
		this.success = new EventEmitter<any>();
		this.error = new EventEmitter<any>();
	}

	initDropzone() {

		this._dropzone = new Dropzone(this.element, {
			url: this.url,
			uploadMultiple: this.uploadMultiple,
			maxFilesize: this.maxFileSize,
			clickable: this.clickable,
			autoProcessQueue: this.autoProcessQueue,
			addRemoveLinks: this.addRemoveLinks,
			createImageThumbnails: this.createImageThumbnails,
			// previewTemplate: this.previewTemplate,
			acceptedFiles: this.acceptedFiles,
			params: {}
		});

		this._dropzone.on("sending", (file, xhr, formaData)=> {

			this.sending.emit(true);

		});

		this._dropzone.on("uploadprogress", (file, progress, bytesSent)=> {

			this.uploadprogress.emit(progress);

		});

		this._dropzone.on("success", (file, savedObject)=> {

			this.success.emit({file, savedObject});

		});

		this._dropzone.on("error", (file, message)=> {

			this.error.emit(message);

		});


	}

	ngOnInit() {
		this.initDropzone();

	}

}