import {NgModule} from '@angular/core';

import {DropzoneComponent} from './dropFile.component';
import {HelloComponent} from './hello.component';

@NgModule({
	declarations: [
		HelloComponent,
		DropzoneComponent
	],
	exports:[HelloComponent,DropzoneComponent]
})
export class SharedComponentsModule {
}