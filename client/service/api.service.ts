import { Injectable } from "@angular/core";
import { AuthHttp } from "angular2-jwt";
import { Response } from "@angular/http";
import "rxjs/add/operator/map";

@Injectable()
export class ApiService {

    constructor(private authHttp: AuthHttp) {}

    get(url: string, data?) {

        return this
            .authHttp
            .get(url)
            .map((response: Response) => response.json());
    }

    post(url: string, data) {
        return this
					.authHttp
					.post(url, data)
					.map((response: Response) => response.json());
    }

    put(url: string, data) {
	    return this
		    .authHttp
		    .put(url, data)
		    .map((response: Response) => response.json());
    }

    delete(url: string, data){
	    return this
		    .authHttp
		    .delete(url, { body: data })
		    .map((response: Response) => response.json());
    }
}
