import {Injectable} from "@angular/core";
import {Http, Headers, RequestOptions, Response} from "@angular/http";
import {JwtHelper} from "angular2-jwt";
import "rxjs/add/operator/map";

const TOKEN_NAME = 'id_token';
const TEACHER_ID = 2;
const PUPIL_ID = 1;
const ROLE_NAME = {
	[`${PUPIL_ID}`]: 'pupil',
	[`${TEACHER_ID}`]: 'teacher'
};

@Injectable()
export class UserService {
	constructor(private jwtHelper: JwtHelper, private http: Http) {
	}

	get userInfo() {
		let token = localStorage.getItem(TOKEN_NAME);
		return token ? this.jwtHelper.decodeToken(token) : {};
	}

	get isLogged() {
		let token = localStorage.getItem(TOKEN_NAME);
		return !!token && !this.jwtHelper.isTokenExpired(token);
	}

	get roleName() {
		return ROLE_NAME[this.userInfo.role];
	}

	isTeacher() {
		return +this.userInfo.role === TEACHER_ID;
	}

	isPupil() {
		return +this.userInfo.role === PUPIL_ID;
	}

	login({login, pass}) {
		return this.http.post("/auth/login", JSON.stringify({password: pass, login: login}),
			new RequestOptions({
				headers: new Headers({"Content-Type": "application/json"})
			}))
			.map((res: Response) => res.json())
			.map((res: Response & { jwt: string }) => {
				localStorage.setItem(TOKEN_NAME, res.jwt);
				return this.userInfo;
			});
	}

	signUp({pass, login, role}) {
		return this.http.post("/auth/signup", JSON.stringify({password: pass, login, role}),
			new RequestOptions({
			headers: new Headers({"Content-Type": "application/json"})
		}))
			.map((res: any) => res.json())
			.map((res: Response & { jwt: string }) => {
				localStorage.setItem(TOKEN_NAME, res.jwt);
				return this.userInfo;
			});
	}

	logout(): void {
		localStorage.removeItem("id_token");
	}

	isAuthor(authorId): boolean {
		return this.userInfo.userId === authorId;
	}

}
