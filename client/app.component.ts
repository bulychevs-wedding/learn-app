import {Component, ViewChild} from "@angular/core";
import {SemanticPopupComponent} from "ng-semantic";
import "rxjs/add/operator/map";

import {UserService} from './service/user.service';
import {Router} from "@angular/router";


@Component({
	selector: "app",
	template: `
<div class="ui container">
    <nav class="ui menu inverted teal huge">
        <a routerLink="home" class="item">Главная</a>
        <a routerLink="courses" class="item" *ngIf="isLogged()">Курсы</a>
        <a routerLink="pupils" class="item" *ngIf="isTeacher()">Ученики</a>
				<h3 class="menu center item">{{appName}}</h3>
        <nav class="menu right">
            <a (click)="loginPopup.show($event, {position: 'right center'})" *ngIf="!isLogged()" class="item">Войти</a>
            <a (click)="registerPopup.show($event, {position: 'right center'})" *ngIf="!isLogged()" class="item">Зарегистрироваться</a>
            <a (click)="userPopup.show($event, {position: 'right center'})" *ngIf="isLogged()" class="item">{{userName}}</a>
            <a (click)="logout()" *ngIf="isLogged()" class="item inverted red">Выйти</a>
        </nav>
    </nav>
   
	        
	      
		<sm-popup class="huge" #userPopup>
			  {{userName}} Вы вошли как {{roleName}}
			  <div>
				    <a routerLink="pupils/{{userId}}" class="item" *ngIf="!isTeacher()">Мои курсы</a>
		    </div>
		</sm-popup>
		
    <sm-popup class="huge" #loginPopup>
        <sm-card class="card basic">
            <card-title> Вход </card-title>
            <card-subtitle>  </card-subtitle>
            
            <card-content>
                <div class="field">
			            <label>Логин</label>
			            <input type="text" [(ngModel)]="loginModel.login" name="login">
			          </div>
			          <div class="field">
			            <label>Пароль</label>
			            <input type="password" name="password" [(ngModel)]="loginModel.password">
			          </div>
            </card-content>

            <sm-button class="bottom attached fluid primary" *ngIf="!isLogged()" (click)="login()">Войти</sm-button>
        </sm-card>
    </sm-popup>
    
    <sm-popup class="huge" #registerPopup>
   			 <sm-card class="card basic">
            <card-title> Ригистрация </card-title>
            <card-subtitle>  </card-subtitle>
            <card-content>
            
            <sm-select
						    [options]="{direction: 'upward'}" 
						    [(model)]="registerModel.role" placeholder="Select role" 
						    class="fluid">
						    <option value="1"><i class="icon student"></i> Pupil</option>
						    <option value="2"><i class="icon doctor"></i> Teacher</option>
						</sm-select>
										  
                <div class="field">
			            <label>логин</label>
			            <input type="text" [(ngModel)]="registerModel.login" name="login">
			          </div>
			          <div class="field">
			            <label>пароль</label>
			            <input type="password" name="password" [(ngModel)]="registerModel.password">
			          </div>
            </card-content>

            <sm-button class="bottom attached fluid primary" (click)="signUp()">Зарегистрироваться</sm-button>
        </sm-card>
       
    </sm-popup>

    <router-outlet></router-outlet>

    

</div>`
})
export class AppComponent {
	appName: string = 'Learn app';

	registerModel: any = {role: '1'};
	loginModel: any = {};

	@ViewChild("loginPopup") loginPopup: SemanticPopupComponent;
	@ViewChild("registerPopup") registerPopup: SemanticPopupComponent;
	@ViewChild("userPopup") userPopup: SemanticPopupComponent;

	constructor(private userService: UserService, private router: Router) {
	}

	signUp() {
		this.userService.signUp({
			pass: this.registerModel.password,
			login: this.registerModel.login,
			role: +this.registerModel.role
		})
			.subscribe(
				(userInfo) => {
					this.registerPopup.hide();
					this.registerModel = {role: '1'};
					console.log(userInfo);
				},
				(error: Error) => {
					console.log(error);
				}
			);
	}

	login() {
		this.userService.login({
			pass: this.loginModel.password,
			login: this.loginModel.login
		})
			.subscribe(
				() => {
					this.loginPopup.hide();
					this.loginModel = {};
				},
				(error: Error) => {
					console.log(error);
					alert(error.message);
				}
			);
	}

	logout(): void {
		this.userService.logout();
		this.router.navigate(['/']);
	}

	isLogged() {
		return this.userService.isLogged;
	}

	isTeacher() {
		return this.userService.isTeacher();
	}

	get userName() {
		return this.userService.userInfo.login;
	}

	get roleName() {
		return this.userService.roleName;
	}

	get userId() {
		return this.userService.userInfo.userId;
	}
}