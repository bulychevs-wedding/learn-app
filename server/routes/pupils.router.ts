import {Router} from 'express';
import {fetch, getMyPupils, mentorPupil, addCurrentCourse, getById} from "../controllers/pupils.ctrl";
import {teacherPermissions} from "../services/user.service";

const pupilsRouter: Router = Router();

// pupilsRouter.post('/', create);
pupilsRouter.get('/', teacherPermissions, fetch);
pupilsRouter.get('/my', teacherPermissions, getMyPupils);
pupilsRouter.put('/mentor', teacherPermissions, mentorPupil);
pupilsRouter.put('/suggest', teacherPermissions, addCurrentCourse);
// pupilsRouter.delete('/:id', deleteLesson);
pupilsRouter.get('/:id', getById);
//
// filesRouter.get('/:id', read);

export {pupilsRouter}
