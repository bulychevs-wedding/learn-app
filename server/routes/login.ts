import {Router} from "express";

import {signUp, login} from "../controllers/login.ctrl";

const loginRouter: Router = Router();

loginRouter.post("/signup", signUp);

// login method
loginRouter.post("/login", login);

export {loginRouter}
