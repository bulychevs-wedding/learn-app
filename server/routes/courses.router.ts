import { Router } from 'express';
import { create, fetch, getById, deleteCourse, getLessons, getCurrent } from '../controllers/courses.ctrl';
import { teacherPermissions } from '../services/user.service';

const coursesRouter: Router = Router();
coursesRouter.post('/', teacherPermissions, create);
coursesRouter.delete('/:id', teacherPermissions, deleteCourse);
coursesRouter.get('/info', getCurrent);
coursesRouter.get('/', fetch);
coursesRouter.get('/:id', getById);
coursesRouter.get('/:id/lessons', getLessons);

export {coursesRouter};

