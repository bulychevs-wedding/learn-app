import {Router} from 'express';
import {create, fetch, getById, deleteLesson, markLearned} from '../controllers/lessons.ctrl';
import {teacherPermissions} from "../services/user.service";

const lessonsRouter: Router = Router();

lessonsRouter.post('/', teacherPermissions, create);
lessonsRouter.get('/', fetch);
lessonsRouter.delete('/:id', deleteLesson);
lessonsRouter.get('/:id', getById);
lessonsRouter.put('/:id/learned', markLearned);
//
// filesRouter.get('/:id', read);

export {lessonsRouter}
