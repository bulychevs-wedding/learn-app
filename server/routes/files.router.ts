import {Router} from 'express';
import { create, read } from '../controllers/files.ctrl';

const filesRouter: Router = Router();

filesRouter.post('/upload', create);

filesRouter.get('/:id', read);

export {filesRouter}
