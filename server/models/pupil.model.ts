import {Schema, Document, model, Types} from 'mongoose';
import {modelName as userModelName} from './user.model';
import {modelName as courseModelName} from './course.model';
import {modelName as currentCourseModelName} from './current-course.model';

export const modelName: string = 'Pupil';

export interface IPupil {
	name?: string;
	auth: string;
	recentCourses?: [Types.ObjectId];
	currentCourses?: [Types.ObjectId];
}


export let pupilSchema: Schema = new Schema({
	name: String,
	auth: {type: Schema.Types.ObjectId, ref: userModelName},
	recentCourses: [{type: Schema.Types.ObjectId, ref: courseModelName }],
	currentCourses: [{type: Schema.Types.ObjectId, ref: currentCourseModelName }]
});


interface IPupilModel extends IPupil, Document {
}

/**
 * Mongoose.Model
 * @type {Model<IPupil>}
 * @private
 */
export let Pupil = model < IPupilModel >(modelName,pupilSchema);
