import {Schema, Document, model, Types,} from 'mongoose';
import {modelName as userModelName} from './user.model';
import {modelName as coursesModelName} from './course.model';
import {modelName as pupilModelName} from './pupil.model';

export const modelName: string = 'Teacher';

export interface ITeacher {
	name?: string;
	auth: string;
	pupils?: [Types.ObjectId];
	courses?: [Types.ObjectId];
}

export let teacherSchema: Schema = new Schema({
	name: String,
	auth: {type: Schema.Types.ObjectId, ref: userModelName},
	pupils: [{type: Schema.Types.ObjectId, ref: pupilModelName}],
	courses: [{type: Schema.Types.ObjectId, ref: coursesModelName}]
});


interface ITeacherModel extends ITeacher, Document {
}

/**
 * Mongoose.Model
 * @type {Model<ITeacher>}
 * @private
 */
export let Teacher = model < ITeacherModel >(modelName,teacherSchema);
