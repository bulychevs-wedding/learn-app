import {Schema, Document, model, Types} from 'mongoose';
import {modelName as userModelName} from './user.model';
import {ILesson, modelName as lessonModelName} from './lesson.model';
import {ICourse, modelName as courseModelName} from './course.model';

export const modelName: string = 'CurrentCourse';
export interface ICurrentCourse {
	pupilId: number;
	courseData: number;
	finishedLessons?: [number];
}

export let currentCourseSchema: Schema = new Schema({
	pupilId: {type: Schema.Types.ObjectId, ref: userModelName },
	courseData: {type: Schema.Types.ObjectId, ref: courseModelName },
	finishedLessons: [{type: Schema.Types.ObjectId, ref: lessonModelName }]
});


interface ICurrentCourseModel extends ICurrentCourse, Document {
}

/**
 * Mongoose.Model
 * @type {Model<ICurrentCourse>}
 * @private
 */
export let CurrentCourse = model < ICurrentCourseModel >(modelName,currentCourseSchema);
