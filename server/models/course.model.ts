import {Schema, Document, model, Types} from 'mongoose';
import {modelName as lessonsModelName} from './lesson.model';
import {modelName as teacherModelName} from "./teacher.model";

export const modelName: string = 'Course';

export interface ICourse {
	name: string;
	description?: string;
	lessons?: [Types.ObjectId];
	author: Types.ObjectId;
}

export let courseSchema: Schema = new Schema({
	name: {
		type: String,
		required: true
	},
	description: String,
	lessons: [{type: Schema.Types.ObjectId, ref: lessonsModelName}],
	author: {type: Schema.Types.ObjectId, ref: teacherModelName }
});


interface ICourseModel extends ICourse, Document {
}

/**
 * Mongoose.Model
 * @type {Model<ICourse>}
 * @private
 */
export let Course = model < ICourseModel >(modelName, courseSchema);
