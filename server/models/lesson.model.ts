import {Schema, Document, model} from "mongoose";

export const modelName: string = 'Lesson';
export interface ILesson {
	name: string;
	description?: string;
	videoUrl?: string;
	videoId?: string;
}

export let lessonSchema: Schema = new Schema({
	name: String,
	description: String,
	videoUrl: String,
	videoId: String
});


interface ILessonModel extends ILesson, Document {
}


/**
 * Mongoose.Model
 * @type {Model<ILesson>}
 * @private
 */
export let Lesson = model < ILessonModel >(modelName, lessonSchema);
