import { Schema, Document, model } from "mongoose";

export const modelName: string = 'User';

export interface IUser {
  login: string;
  pass: string;
  salt: string;
  role: number; // 1 - pupil; 2 - teacher;
}

export let userSchema: Schema = new Schema({
    login: {
      type: String,
      required: true,
      unique: true
    },
    pass: String,
    salt: String,
    role: Number
  });


 interface IUserModel extends IUser, Document { }
  /*.pre('save', function(next) {
    this.updated = new Date();
    next();
  })*/;

/**
 * Mongoose.Model
 * @type {Model<IUser>}
 * @private
 */
export let User = model < IUserModel > (modelName, userSchema);