import * as mongoose from "mongoose";
import { dbUrl } from '../config';

export let connectDb = function() {
	mongoose.Promise = global.Promise;
	mongoose.connect(dbUrl);

	let db = mongoose.connection;

	db.on('open', () => {
		console.log('db connected');
	});
	db.once('error', (e) => {
		console.error('error connection', e);
	});
};