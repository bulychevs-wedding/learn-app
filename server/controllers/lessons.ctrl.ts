import {Request, Response, NextFunction} from "express";
import {Lesson} from '../models/lesson.model';
import {Course} from "../models/course.model";
import {CurrentCourse} from "../models/current-course.model";
import {Pupil} from "../models/pupil.model";

export let create = function (req: Request, response: Response, next: NextFunction) {

	Lesson.create({
		name: req.body.name,
		description: req.body.description,
		videoUrl: req.body.videoUrl,
		videoId: req.body.videoId
	})
		.then(lesson => {

			Course
				.findById(req.body.courseId, (err, course) => {
					course.lessons.push(lesson._id);
					course.save()
						.then(lesson => response.json(lesson), err => response.json(err));
				});
		});
};


export let fetch = function (req: Request, response: Response, next: NextFunction) {
	// Lesson.find({}, 'name description')
	Lesson.find({}, (err, lessons) => {
		return err ? response.json(err) : response.json(lessons);
	});
};

export let getById = function (req: Request, response: Response, next: NextFunction) {

	Lesson.findById(req.params.id, (err, lesson) => {
		return err ? response.json(err) : response.json(lesson);
	});
};

export let deleteLesson = function (req: Request, response: Response, next: NextFunction) {
	Course.findById(req.body.courseId)
		.exec()
		.then(course => {
			let lessonIndex = course.lessons.indexOf(req.params.id);
			course.lessons.splice(lessonIndex, 1);
			Lesson.findByIdAndRemove(req.params.id, (err) => {
				return err ? response.json(err) : getLessons(req, response, next);
			});
		});
};

let getLessons = function (req: Request, response: Response, next: NextFunction) {
	Course.findById(req.body.courseId)
		.populate('lessons')
		.exec()
		.then(course => response.json(course.lessons),
			err => response.json(err));
};

export let markLearned = function (req: Request, response: Response, next: NextFunction) {
	Course.findById(req.body.courseId)
		.populate('lessons')
		.exec()
		.then(course => {
			if (!course) {
				response.status(500);

				return response.json(course);
			}

			if(course.lessons.length === 1) {

				Pupil.findById(req.body.userId)
					.then(pupil => {
						pupil.recentCourses.push(req.body.courseId);
						return pupil.save();
					});
			} else {
				CurrentCourse.findOne({
					pupilId: req.body.userId,
					courseData: req.body.courseId
				})
					.exec()
					.then(currentCourse => {
						if (currentCourse) {
							if (course.lessons.length === currentCourse.finishedLessons.length + 1) {
								Pupil.findById(req.body.userId)
									.then(pupil => {
										pupil.recentCourses.push(req.body.courseId);
										let courseIndex = pupil.currentCourses.indexOf(currentCourse.id);
										pupil.currentCourses.splice(courseIndex, 1);
										return pupil.save();
									})
									.then(() => {
										currentCourse.remove()
											.then(response.json(currentCourse));
									});
							}
							console.log('Yes', currentCourse);
							currentCourse.finishedLessons.indexOf(req.params.id) === -1 && currentCourse.finishedLessons.push(req.params.id);
							currentCourse.save()
								.then(course => response.json(course));
						} else {
							console.log('NO');
							CurrentCourse.create({
								pupilId: req.body.userId,
								courseData: req.body.courseId
							})
								.then(currentCourse => {
									currentCourse.finishedLessons.indexOf(req.params.id) === -1 && currentCourse.finishedLessons.push(req.params.id)
									return currentCourse.save();
								})
								.then(course => {
									Pupil.findById(req.body.userId)
										.exec()
										.then(pupil => {
											pupil.currentCourses.push(course._id);
											return pupil.save();
										})
										.then(pupil => response.json(course));


								})
						}
					});
			}
		});
};