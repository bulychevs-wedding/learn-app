import {Request, Response, NextFunction} from "express";
import {Pupil, modelName as pupilModelName} from "../models/pupil.model";
import {Teacher} from "../models/teacher.model";
import {modelName as userModelName} from "../models/user.model";
import {CurrentCourse} from "../models/current-course.model";

export let fetch = function (req: Request, response: Response, next: NextFunction) {
	Pupil.find({})
		.populate('auth', 'login')
		.exec()
		.then(pupils => {
			if (!pupils) {
				response.status(500);
				return response.json({message: 'No pupils'});
			} else {
				return response.json(pupils);
			}
		})
};

export let getMyPupils = function (req: Request, response: Response, next: NextFunction) {
	Teacher.findById(req.body.userId)
	// .populate('pupils')
		.populate({
			path: 'pupils',
			model: pupilModelName,
			populate: {
				path: 'auth',
				select: 'login',
				model: userModelName
			}
		})
		.exec()
		.then(teacher => {
			if (!teacher) {
				response.status(500);
				return response.json({message: 'incorrect teacher'});
			} else {
				return response.json(teacher.pupils);
			}
		})
};

export let mentorPupil = function (req: Request, response: Response, next: NextFunction) {
	Teacher.findById(req.body.userId)
		.exec()
		.then(teacher => {
			if (!teacher) {
				response.status(500);
				return response.json({message: 'incorrect teacher'});
			} else {
				if (teacher.pupils.some(pupilId => pupilId === req.body.pupilId)) {
					response.status(500);
					response.json({message: 'Pupil is already mentored'});
				} else {
					teacher.pupils.push(req.body.pupilId);
					teacher.save()
						.then(teacher => getMyPupils(req, response, next));
				}
			}
		})
};

export let addCurrentCourse = function (req: Request, response: Response, next: NextFunction) {
	Pupil.find({
		'_id': {
			$in: req.body.pupilIds
		}
	}, (err, pupils) => {
		CurrentCourse.find({
			'pupilId': {
				$in: req.body.pupilIds
			}
		}, currentCourses => {
			if (currentCourses) {
				pupils = pupils.filter(pupil => currentCourses
					.some(currentCourse => currentCourse.pupilId !== pupil._id))

			}
		});

		let savings = pupils.map(pupil => {
			return CurrentCourse.create({
				pupilId: pupil._id,
				courseData: req.body.courseId
			})
				.then(currentC => {
					pupil.currentCourses.push(currentC._id);
				})
				.then(() => pupil.save());
		});
		Promise.all(savings).then(() => response.json(pupils));
	});
};

export let getById = function (req: Request, response: Response, next: NextFunction) {
	Pupil.findById(req.params.id)
		.populate('auth', 'login')
		.populate({
			path: 'currentCourses',
			populate: {
				path: 'courseData finishedLessons'
			}
		})
		.populate('recentCourses')
		.exec()
		.then(pupil => response.json(pupil));
};