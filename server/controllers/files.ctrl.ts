import {Request, Response, NextFunction} from "express";

import { mongo, connection, Types} from "mongoose";

import * as Grid from 'gridfs-stream';

Grid.mongo = mongo;
let gfs;
connection.once('open', () => {
	gfs = new Grid(connection.db);
});

export let create = function(req: Request, res: Response, next: NextFunction) {
	let part = req.files.file;
	// let id = new Types.ObjectId();

	let writeStream = gfs.createWriteStream({
		// _id: id,
		filename: part.name,
		mode: 'w',
		content_type:part.mimetype
	});

	writeStream.on('close', file => {
		return res.status(200).send(file);
	});

	writeStream.write(part.data);

	writeStream.end();

};


export let read = function(req: Request, res: Response, next: NextFunction) {
	gfs.files.find({ _id: new Types.ObjectId(req.params.id) }).toArray(function (err, files) {

		if(files.length===0){
			return res.status(400).send({
				message: 'File not found'
			});
		}

		res.writeHead(200, {'Content-Type': files[0].contentType});

		let readstream = gfs.createReadStream({
			_id: files[0]._id
		});

		readstream.on('data', function(data) {
			res.write(data);
		});

		readstream.on('end', function() {
			res.end();
		});

		readstream.on('error', function (err) {
			console.log('An error occurred!', err);
			throw err;
		});
	});

};