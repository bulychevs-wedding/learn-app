import {Request, Response, NextFunction} from "express";
import {Course} from '../models/course.model';
import {getCurrentModel} from '../services/user.service';
import {CurrentCourse} from "../models/current-course.model";
import {Pupil} from "../models/pupil.model";

export let create = function (req: Request, response: Response, next: NextFunction) {
	let currentModel = getCurrentModel(req.body);

	Course
		.create({
			name: req.body.name,
			description: req.body.description,
			author: req.body.userId
		})
		.then(course => {
			currentModel.findById(req.body.userId, (err, user) => {
				user.courses.push(course._id);
				user.save()
					.then(user => {
						return err ? response.json(err) : response.json(course);
					}, err => response.json(err));
			})
		});
};


export let fetch = function (req: Request, response: Response, next: NextFunction) {
	Course
		.find({}, (err, lessons) => {
			return err ? response.json(err) : response.json(lessons);
		});
};

export let getById = function (req: Request, response: Response, next: NextFunction) {

	Course.findById(req.params.id, (err, course) => {
		return err ? response.json(err) : response.json(course);
	});
};

export let deleteCourse = function (req: Request, response: Response, next: NextFunction) {
	let currentModel = getCurrentModel(req.body);

	currentModel.findById(req.body.userId, (err, user) => {
		let courseIndex = user.courses.indexOf(req.params.id);
		user.courses.splice(courseIndex, 1);
		user.save()
			.then(user => {
				Course.findByIdAndRemove(req.params.id, (err) => {
					return err ? response.json(err) : fetch(req, response, next);
				});
			});

	});
};

export let getLessons = function (req: Request, response: Response, next: NextFunction) {
	Course.findById(req.params.id)
		.populate('lessons')
		.exec()
		.then(course => response.json(course),
			err => {
				response.status(500);
				response.json(err)
			});
};

export let getCurrent = function (req: Request, response: Response, next: NextFunction) {
	let currentModel = getCurrentModel(req.body);
	if (req.query.courseId) {
		return CurrentCourse.findOne({
			pupilId: req.query.userId,
			courseData: req.query.courseId
		})
			.exec()
			.then(course => {
				if (course) {
					return response.json(course);
				} else {
					let course = {};
					Pupil.findById(req.query.userId)
						.exec()
						.then(pupil => {
							course.isFinished = pupil.recentCourses.some(recent => {
								console.log('recent', recent);
								return recent == req.query.courseId;
							});
							return response.json(course);
						});
				}
			});
	} else {
		currentModel.findById(req.query.userId)
			.populate('currentCourses')
			.populate('recentCourses')
			.exec()
			.then(pupil => response.json(pupil),
				err => {
					response.status(500);
					response.json(err)
				});
	}
};
