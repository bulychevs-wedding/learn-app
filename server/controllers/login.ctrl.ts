import {Request, Response, NextFunction} from "express";
import {randomBytes, pbkdf2} from "crypto";
import {sign} from "jsonwebtoken";

import {secret, length, digest} from "../config";
import {getCurrentModel} from '../services/user.service';

import {User} from '../models/user.model';


export let signUp = function (request: Request, response: Response, next: NextFunction) {
	let currentModel = getCurrentModel(request.body);

	if (!currentModel) {
		let err = new Error('Wrong Role');

		return next(err);
	}

	if (!request.body.hasOwnProperty("password")) {

		let err = new Error("No password");
		return next(err);
	}
	const salt = randomBytes(128).toString("base64");

	pbkdf2(request.body.password, salt, 10000, length, digest, (err: Error, hash: Buffer) => {

		User.findOne({login: request.body.login})
			.exec()
			.then(user => {
				if (user) {
					response.status(409);
					response.json(new Error('This login name is already used'));
				} else {
					return User
						.create({
							login: request.body.login,
							pass: hash.toString('hex'),
							salt,
							role: request.body.role
						})
						.then(createdUser => currentModel.create({
							auth: createdUser._id
						})
							.then(roledUser => response.json({
								jwt: sign({
									login: createdUser.login,
									role: createdUser.role,
									userId: roledUser._id,
									permissions: []
								}, secret, {expiresIn: "7d"})
							})));
				}
			});
	});

};

export let login = function (request: Request, response: Response, next: NextFunction) {
	User.findOne({login: request.body.login})
		.exec()
		.then(user => {
			if (!user) {
				response.status(401);
				response.json({message: 'Unothorized'});
			}
			let currentModel = getCurrentModel(user);

			currentModel
				.findOne({
					auth: user._id
				})
				.exec()
				.then(roledUser => {
					if (!roledUser) {
						response.status(401);
						return response.json({message: 'The wrong role', err});
					}

					pbkdf2(request.body.password, user.salt, 10000, length, digest, (err: Error, hash: Buffer) => {
						if (err) {
							console.log(err);
						}

						// check if password is active
						if (hash.toString("hex") === user.pass) {

							const token = sign({
								login: user.login,
								role: user.role,
								userId: roledUser._id,
								permissions: []
							}, secret, {expiresIn: "7d"});
							response.json({"jwt": token});

						} else {
							response.status(401);
							response.json({message: "Wrong password"});
						}
					});
				});
		});
};

