import {verify} from "jsonwebtoken";
import {secret} from "../config";
import {Pupil} from '../models/pupil.model';
import {Teacher} from '../models/teacher.model';
import {Router, Response, Request, NextFunction} from "express";

const PUPIL_ID = 1;
const TEACHER_ID = 2;

function getUserInfo(token) {
	return  verify(token, secret);
}


function getCurrentModel(data) {
	let currentModel;

	switch (data.role) {
		case PUPIL_ID:
			currentModel = Pupil;
			break;
		case TEACHER_ID:
			currentModel = Teacher;
			break;
	}
	return currentModel;
}

function teacherPermissions(request: Request & {headers: {authorization: string}}, response: Response, next: NextFunction) {
	const token = request.headers.authorization;
	console.log('midleware: ' + getUserInfo(token).role);
	getUserInfo(token).role === 2 ? next() : response.status(403).json({
			message: 'You have no permissioins for this action'
		});
}

export {getUserInfo, getCurrentModel, teacherPermissions};
